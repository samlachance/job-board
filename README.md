# Job Board

## Software used

- Elixir 1.7.x
- postgres
- Phoenix

## App setup

Install hex:

```
mix local.hex
```

Resolve dependencies:

```
mix deps.get
```

```
npm install
```

## Database setup

Create postgres role:

```
$ psql

> CREATE ROLE jobboard WITH LOGIN CREATEDB PASSWORD 'jobboard';

=> Role created
```

Create and migrate db:

```
$ mix ecto.create && mix ecto.migrate
```

## Run server

```
mix phoenix.server
```
