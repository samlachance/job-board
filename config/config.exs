# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.
use Mix.Config

# General application configuration
config :job_board,
  ecto_repos: [JobBoard.Repo]

# Configures the endpoint
config :job_board, JobBoard.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "8mw4uzS+/tgss3NT2vJ+vNqhwdA7/UsnlC0sS/+XeJiNs5s2hp3v9K5yAZIvpIqJ",
  render_errors: [view: JobBoard.ErrorView, accepts: ~w(html json)],
  pubsub: [name: JobBoard.PubSub,
           adapter: Phoenix.PubSub.PG2]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env}.exs"
