use Mix.Config

# We don't run a server during test. If one is required,
# you can enable the server option below.
config :job_board, JobBoard.Endpoint,
  http: [port: 4001],
  server: false

# Print only warnings and errors during test
config :logger, level: :warn

# Configure your database
config :job_board, JobBoard.Repo,
  adapter: Ecto.Adapters.Postgres,
  username: System.get_env("TEST_DB_USER") || "postgres",
  password: System.get_env("TEST_DB_PASS") || "postgres",
  database: System.get_env("TEST_DB_NAME") || "job_board_test",
  hostname: System.get_env("TEST_DB_HOST") || "postgres",
  pool: Ecto.Adapters.SQL.Sandbox
